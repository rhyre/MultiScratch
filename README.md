### Wrapper for FromScratch https://fromscratch.rocks/

FromScratch supports only a single workspace.
MultiScratch aims to address that and adds multi project support.

1. To install run install.sh.

2. Use like:
  * ```createscratch -cdl projectName```
    * -c - create 
    * -d - delete
    * -l - list (with this you don't have to specify project name)
  * ```usescratch [projectName]```

3. Some other notes
  * Instead of usescratch command, you can use a gnome/unity launcher named "FromScratch for ProjectName" which will fire projects workspace. The launcher will appear after createscratch command execution.
  * Content and folding conf of FromScratch will be preserved throughout projects.